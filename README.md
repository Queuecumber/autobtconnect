Automatic bluetooth connections with BlueZ and PyDbus

This python file is designed to run as a system service and a systemd unit file 
is provided to make it easier to do so. Note that this systemd unit file makes 
a few assumptions. As this manager is intended for headless embedded systems, it
assumes that pulseaudio runs as a system level daemon spawned by systemd, and it
waits for this to happen so that bluetooth audio connections are handled 
properly. The unit file also assumes that the python script is installed to 
/usr/local/bin, if you want to put the script somewhere else make sure the unit
file is updated appropriately.

The managers behavior is, on start, to try to reconnect to any previously 
paired bluetooth device. If no such devices can be connected (or there arent 
any) it goes into dicoverable pairing mode with a blank password and waits for 
someone to connect. Provided there is a connection which is later disconnected, 
the manager falls back to the discoverable pairing mode. 