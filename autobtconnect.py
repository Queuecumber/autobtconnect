#!/usr/bin/python
import dbus
import dbus.mainloop.glib
import gobject
import re
import sys
from xml.dom import minidom
import time

gobject.threads_init()

managed_devices = {}


def property_changed(self, props, other, path, interface):
    iface = interface[interface.rfind(".") + 1:]
    print >> sys.stderr, "{%s.PropertiesChanged} [%s] %s" % (iface, path, props)

    if re.match("/org/bluez/hci[0-9]$", path):
        if path not in managed_devices:
            managed_devices[path] = False

        if "Powered" in props and props.get("Powered"):
            auto_connect(path)
        elif path in managed_devices and not managed_devices[path]:
            auto_connect(path)

    if 'Connected' in props and not props.get('Connected'):
        discoverable()


def interface_added(object_path, iface_and_property, path, interface):
    iface = interface[interface.rfind(".") + 1:]
    val = str(iface_and_property)
    print >> sys.stderr, "{%s.InterfacesAdded} [%s] %s" % (iface, path, object_path)

    if re.match("/org/bluez/hci[0-9]$", object_path):
        managed_devices[object_path] = False

    if re.match("/org/bluez/hci[0-9]/dev_[0-9A-F][0-9A-F]_[0-9A-F][0-9A-F]_[0-9A-F][0-9A-F]_[0-9A-F][0-9A-F]_[0-9A-F][0-9A-F]_[0-9A-F][0-9A-F]$", object_path) and path == '/':
        print >> sys.stderr, "Setting up new device %s", object_path
        device = bus.get_object("org.bluez", object_path)

        prop_manager = dbus.Interface(device, 'org.freedesktop.DBus.Properties')
        prop_manager.Set('org.bluez.Device1', 'Trusted', True)

        iface = dbus.Interface(device, 'org.bluez.Device1')
        iface.Pair()

def discoverable():
    bzobj = bus.get_object("org.bluez", managed_devices.keys()[0])

    print >> sys.stderr, "Unable to connect to any paired devices, going into discovery mode"
    prop_manager = dbus.Interface(bzobj, 'org.freedesktop.DBus.Properties')
    prop_manager.Set('org.bluez.Adapter1', 'Discoverable', True)
    prop_manager.Set('org.bluez.Adapter1', 'Pairable', True)


def auto_connect(path):
    try:
        print >> sys.stderr, "Found BT Device at %s, Connecting" % path
        bzobj = bus.get_object("org.bluez", path)

        prop_manager = dbus.Interface(bzobj, 'org.freedesktop.DBus.Properties')
        prop_manager.Set('org.bluez.Adapter1', 'Powered', True)

        idata = bzobj.Introspect()

        xmldoc = minidom.parseString(idata)

        items = xmldoc.getElementsByTagName('node')

        got_connection = False
        for i in items:
            if i.attributes.has_key('name'):
                if i.attributes['name'].value.startswith('dev_'):
                    device = bus.get_object("org.bluez", path + "/" + i.attributes['name'].value)
                    iface = dbus.Interface(device, 'org.bluez.Device1')

                    try:
                        res = iface.Connect()
                    except dbus.exceptions.DBusException as e:
                        if not e.get_dbus_name() == 'org.freedesktop.DBus.Error.NoReply':
                            print >> sys.stderr, "Connection to %s failed: %s" % (i.attributes['name'].value, e)
                            continue
                        else:
                            print >> sys.stderr, "Got NoReply, hoping that means the connection worked..."

                    got_connection = True
                    break

        managed_devices[path] = True

        if not got_connection:
            discoverable()

    except dbus.exceptions.DBusException as e:
        print >> sys.stderr, e


print >> sys.stderr, "Starting Up AutoBTConnect"
dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

bus = dbus.SystemBus()

bus.add_signal_receiver(interface_added, bus_name="org.bluez", signal_name="InterfacesAdded", path_keyword="path",
                        interface_keyword="interface")
bus.add_signal_receiver(property_changed, bus_name="org.bluez", signal_name="PropertiesChanged", path_keyword="path",
                        interface_keyword="interface")

auto_connect('/org/bluez/hci0')

loop = gobject.MainLoop()
loop.run()
